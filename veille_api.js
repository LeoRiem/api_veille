
var veilles = []; //Je crée un tableau dans une variable pour recevoir le contenu du JSON

//Le jsoncallback dans lequel je génére les boutons des veilles avec leurs titres troncatés
function jsoncallback(data){
  veilles = data.veilles;

  $(function() {
    $("#pagination").pagination({
      items: veilles.length,
      itemsOnPage: 28,
      cssStyle: 'light-theme'
    });
  });

  count_up = veilles.length
  count_down = veilles.length -28

  veilles = veilles.reverse();

  veilles.forEach(function(veille) {
     count_up--;
     if (count_up >= count_down) {
       $("#button").append("<button type='button' onclick='load_veille(" + veille.id + ")'>" + trunc(veille.title) + "</button>");
     }
   });
}

//Récupération du JSON
$(document).ready(function() {
   $.getJSON("http://veille.popschool.fr/api/?api=veille&action=getAll",jsoncallback);
});

//Charge le contenu de la veille en fonction de l'id du bouton
function load_veille(id_veille) {
    empty();

    //Id match
    veilles.forEach(function(veille) {
      if(veille.id == id_veille) {

        //Lorsque la veille est chargé, fait apparaitre la div auteur et le bouton "update"
        $("#author_show").show();
        $("#update").show();

        //Pour chaque élément de la veille, rempli la div concerné
        $("#id").text("Id : "+veille.id);
        $("#title").text("Titre : "+veille.title);
        $("#date").text("Date : "+veille.date);
        $("#type").text("Type : "+veille.type);
        $("#description").text("Description : "+veille.description);
        $("#link").text("Lien : "+veille.link);
        $("#source").text("Source : "+veille.source);
        $("#id_user").text(veille.id_user);
        $("#firstname").text(veille.firstname);
        $("#filename").text(veille.filename);

        //Pour chaque élément de la veille, rempli l'input concerné
        $("#id_input").val(veille.id);
        $("#title_input").val(veille.title);
        $("#date_input").val(veille.date);
        $("#type_input").val(veille.type);
        $("#description_input").val(veille.description);
        $("#link_input").val(veille.link);
        $("#source_input").val(veille.source);
        $("#id_user_input").val(veille.id_user);
        $("#firstname_input").val(veille.firstname);
        $("#filename_input").val(veille.filename);
        console.log(veille);
      }
    });
}

//Modifie l'affichage des boutons et affiche le formulaire en appuyant sur le bouton pour modifier la veille
function update_form() {
  $("#form").show();
  $("#abort").show();
  $("#update").hide();
}

//Modifie l'affichage des boutons et affiche le formulaire en appuyant sur le bouton pour créer la veille
function load_form() {
  $("#form").show();
  $("#abort").show();
  empty();
}

//Reviens à l'état initial
function abort() {
  $("#form").hide();
  $("#abort").hide();
  empty();
}

//Fonction appelé pour vider le contenu du formulaire et des divs contenant les détails de la veille sélectionné
function empty() {
  $("#title_input").val('');
  $("#id_input").val('');
  $("#id__user_input").val('');
  $("#date_input").val('');
  $("#type_input").val('');
  $("#description_input").val('');
  $("#link_input").val('');
  $("#source_input").val('');
  $("#firstname_input").val('');
  $("#filename_input").val('');

  $("#title").empty();
  $("#id").empty();
  $("#date").empty();
  $("#type").empty();
  $("#description").empty();
  $("#link").empty();
  $("#source").empty();
  $("#filename").empty();
  $("#author_show").hide();
}

//Fonction pour troncater le titre
function trunc(elem) {
  if(elem.length > 13) {
    return elem.substr(0,13).concat('...');
  } else {
    return elem
  }
}

//Enregistrement de la veille contenu dans le formulaire
function save() {
    var f = new Object();
    f.title = $("#title_input").val();
    f.id = $("#id_input").val();
    f.date = $("#date_input").val();
    f.type = $("#type_input").val();
    f.description = $("#description_input").val();
    f.link = $("#link_input").val();
    f.source = $("#source_input").val();
    f.firstname = $("#firstname_input").val();
    f.id_user = $("#id_user_input").val();
    f.filename = $("#filename_input").val();
    console.log(f)

    //Demande les infos de l'utilisateur
    email_user = prompt("e-mail : ","");
    if(f.firstname == "") {
      f.firstname = prompt("Prénom ?","Philippe");
    }
    if(f.id_user == '') {
      f.id_user = prompt("Votre id ?","1");
    }
    password_user = prompt("password : ","");

    //Envois des données
    $.ajax({
      url: "http://veille.popschool.fr/api/",
      data: {api: "veille", action:"save", email: email_user, password: password_user, veille:JSON.stringify(f)},
      complete: function(r) {
        console.log(r.responseText);
        setTimeout(function() { $(location).attr('href','index.html'); }, 1500);
      }
    });
  }
